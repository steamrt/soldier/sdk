# Building in the Steam Runtime environment using Distrobox

[Distrobox](https://distrobox.it/) is a tool to set up convenient
containerized command-line environments.
It wraps either [Podman](podman.md), [Docker](docker.md) or Lilipod,
but provides more convenient sharing between the container and the
host operating system,
similar to [Toolbx](toolbx.md).
It is available in recent versions of most Linux distributions,
usually as a package named `distrobox`.

Like Toolbx, Distrobox can be used to compile games in a predictable,
consistent environment, with a relatively complete set of tools and
libraries included.

To download the container image and set up a Distrobox container,
on a system where Distrobox uses Podman,
use a command like this:

    distrobox create -i registry.gitlab.steamos.cloud/steamrt/soldier/sdk soldier

To enter the container environment, use a command like:

    distrobox enter soldier

You can download and install new packages using `sudo apt-get`,
the same as you would in an ordinary Debian or Ubuntu system.

Using Distrobox with Docker is not documented here,
and is likely to require root privileges.
We suggest preferring Podman over Docker for interactive use.

## Not a security boundary

Please note that Distrobox is not designed to put a security boundary
between programs in the container and programs on the host.
Anything that you run inside the container has full access to your home
directory and privileges on the host system.
As a result, it is not safe to run potentially-malicious code inside
the Distrobox container.
If isolation between the container and the host is required,
use [Podman](podman.md) or [Docker](docker.md) instead.

## Beta SDK

To use the beta SDK, specify the `beta` tag:

    distrobox create -i registry.gitlab.steamos.cloud/steamrt/soldier/sdk:beta soldier

Games compiled in the beta SDK will not necessarily work with the
general-availability version of the container runtime.
See the [README](../README.md) for more information about the beta SDK.

## Mixing Distrobox with lower-level container tools

Distrobox usually acts a wrapper around either [Podman](podman.md) or
[Docker](docker.md),
so it is possible to combine the two tools,
for example using Distrobox for interactive builds and debugging,
then switching to Podman for a final "clean" build that is better-isolated
from the host system.

In particular, if your network connection is slow or unreliable, you
might find it useful to pre-download the container image using Podman,
which has better progress reporting:

    podman pull registry.gitlab.steamos.cloud/steamrt/soldier/sdk
