# soldier SDK

This project provides container images for the Steam Runtime 2 'soldier'
SDK.

For general information about soldier, please see the README in the
`steamrt/soldier` branch of the `steamrt` package:
<https://gitlab.steamos.cloud/steamrt/steamrt/-/blob/steamrt/soldier/README.md>.

## Developing software that runs in soldier

Native Linux games that require soldier cannot be released on Steam.
The next-generation runtime for native Linux games is intended to be
[Steam Runtime 3 `sniper`][sniper SDK].
All older native Linux games should be compiled for
[Steam Runtime 1 `scout`][scout SDK].
The soldier SDK is primarily useful for Proton developers.

We recommend compiling in an OCI-based container using a framework such
as Docker or Podman.
This ensures that only the libraries in the container are used for
compilation, as well as making builds more predictable and reproducible.
See [below](#oci) for more details.

Previous versions of the Steam Runtime, published before container
frameworks had become widespread, recommended using a
[chroot](https://en.wikipedia.org/wiki/Chroot)
environment via the `schroot` tool.
We no longer recommend this, because it is more difficult to set up on
non-Debian-derived machines and requires root privileges, but it is
still possible: see [below](#tar).

A [guide for game developers][] is available.

## OCI images for Docker and Podman <a name="oci"></a>

For Docker ([more information](doc/docker.md)):

    sudo docker pull registry.gitlab.steamos.cloud/steamrt/soldier/sdk

or for [Podman](https://podman.io/) ([more information](doc/podman.md)):

    podman pull registry.gitlab.steamos.cloud/steamrt/soldier/sdk

or for [Toolbx](https://containertoolbx.org/)
([more information](doc/toolbx.md)):

    toolbox create -i registry.gitlab.steamos.cloud/steamrt/soldier/sdk soldier
    toolbox enter soldier

or for [Distrobox](https://distrobox.it/)
([more information](doc/distrobox.md)):

    distrobox create -i registry.gitlab.steamos.cloud/steamrt/soldier/sdk soldier
    distrobox enter soldier

`registry.gitlab.steamos.cloud/steamrt/soldier/sdk` is the 64-bit SDK.
Most libraries also have development files available for 32-bit builds.
The development files for a few libraries that use legacy `-config` scripts
(`libfltk1.1-dev`, `libgcrypt20-dev` and `libgpg-error-dev`) are currently
64-bit-only.

There is no separate 32-bit SDK for soldier.

Tags available for this repository (can be specified after `:` when using
`docker pull`):

* `2.0.20250108.112706` (etc.): Specific versions of the SDK. Use one of these
    if you need to "pin" to an older version for reproducible builds.
    These version numbers correspond exactly to
    <https://repo.steampowered.com/steamrt2/images/2.0.20250108.112706/>
    and so on.

* `latest`, `latest-container-runtime-depot`:
    An alias pointing to the SDK corresponding to the runtime that is included
    in the current public stable version of the `SteamLinuxRuntime_soldier` depot.
    If in doubt, build against this.
    This should always match the version in
    <https://repo.steampowered.com/steamrt2/images/latest-container-runtime-depot/>.

* `beta`, `latest-container-runtime-public-beta`:
    An alias pointing to the SDK corresponding to the runtime that is included
    in the most recent public beta version of the `SteamLinuxRuntime_soldier`
    depot. This should always match the version in
    <https://repo.steampowered.com/steamrt2/images/latest-container-runtime-public-beta/>.
    If there is currently no public beta available, then this version will
    usually be the same as `latest`, or it might even be older than `latest`.
    Only use this if you know that it is what you want.

These OCI images were prepared using Docker, but should be equally suitable
for other OCI-compatible tools like `podman`.

## Tar archives for schroot and other non-OCI environments <a name="tar"></a>

We recommend Docker as the preferred way to develop against soldier.
However, for those who are more familiar with the `schroot` tool or other
non-OCI-based chroot and container environments, the `-sysroot.tar.gz`
archives available from
<https://repo.steampowered.com/steamrt2/images/>
contain the same packages as the official OCI images.
[More information about schroot](doc/schroot.md)

If you prefer other chroot or container frameworks such as
`systemd-nspawn`, the same tar archives can be unpacked into a directory
and used with those.

The versions available are the same as for the [OCI images](#oci).

## Toolchains

Several C/C++ compilers are available in the soldier SDK:

* gcc-5 and g++-5
* gcc-8 and g++-8 (default)
* gcc-9 and g++-9
* gcc-12 and g++-12 (experimental)
* clang-7 and clang++-7

The experimental `gcc-12` and `g++-12` can be installed by using
`apt-get install gcc-12-monolithic` but are not currently included in
the SDK itself.

To avoid an unexpected upgrade of the Standard C++ library, `gcc-9`,
`g++-9` and newer versions always behave as though the `-static-libgcc` and
`static-libstdc++` options had been used.

All C++ code in your project should usually be built with the same compiler.
C-based external dependencies such as SDL and GLib are safe to use, but
passing a STL object such as `std::string` to C++-based libraries from
the Steam Runtime or the operating system (for example `libgnutlsxx` or
`libpcrecpp`) will not necessarily work and is best avoided, especially
if you are using `g++-9` or newer.

Most build systems have a way to use a non-default compiler by specifying
its name, for example Autotools `./configure CC=gcc-5 CXX=g++-5 ...` and
CMake `cmake -DCMAKE_C_COMPILER=gcc-5 -DCMAKE_CXX_COMPILER=g++-5 ...`.

Meson users can use a command like `meson --native-file=gcc-5.txt ...`
to select a different compiler.
See `/usr/share/meson/native` in the container for all the options available.

### binutils

There are also two implementations of the `binutils` linker, assembler, etc.:

* binutils 2.31 (default)
* binutils 2.35

To use binutils 2.35, prepend `/usr/lib/binutils-2.35/bin` to the `PATH`:

    (steamrt soldier 2.0.20250108.112706):~$ export PATH="/usr/lib/binutils-2.35/bin:$PATH"

or run individual tools using names like `ld-2.35` and `objcopy-2.35`.

`gcc-9`, `g++-9` and newer automatically use binutils 2.35.
Older compilers use binutils 2.31 by default.

### mold linker

An experimental backport of the `mold` linker from Debian 12 is available
for installation via `apt-get install mold` and can be used with
`g++-12 -fuse-ld=mold` or `g++ -B/usr/libexec/mold`.
The `-fuse-ld=mold` command-line option only works with `g++-12` or later.

## 32-bit code

The soldier SDK can compile 32-bit code in two different ways:

* cross-compiler-style, by using prefixed tools such as
    `i686-linux-gnu-gcc` and `i686-linux-gnu-objcopy`;
* multilib-style, by passing `-m32` to the default 64-bit compiler

Both should result in binaries that link to the same libraries.

Meson users can use a command like `meson --cross-file=gcc-m32.txt ...`
to select the `-m32` multilib toolchain.
See `/usr/share/meson/cross` in the container for all the options available.

## apt packages

All of the packages that are supported by the Platform runtime are included
in the SDK image and do not need to be downloaded separately. An apt repository
is also available, and is preconfigured in the OCI images:

    # corresponding to the :latest OCI image
    deb https://repo.steampowered.com/steamrt2/apt soldier main contrib non-free
    deb-src https://repo.steampowered.com/steamrt2/apt soldier main contrib non-free
    # corresponding to the :beta OCI image and not configured by default
    #deb https://repo.steampowered.com/steamrt2/apt soldier_beta main contrib non-free
    #deb-src https://repo.steampowered.com/steamrt2/apt soldier_beta main contrib non-free

soldier is based on, and broadly compatible with, Debian 10 'buster'.
Many packages in the SDK are taken from Debian 10 without modification,
and those packages are not included in the soldier-specific apt repository.

If additional development tools are required, you can install them
from the Debian 10 buster apt repositories. However, please be careful
not to introduce dependencies on packages that are not included in the
soldier Platform. The soldier Platform is the only thing guaranteed to be
available at runtime.

Additional development tools can also be installed from buster-backports,
from third-party apt repositories compatible with Debian 10, or from
source code. Again, please be careful not to introduce dependencies on
packages that are not included in the soldier Platform.

## Source code

Source code for all the packages that go into the OCI image can be found
in the appropriate subdirectory of
<https://repo.steampowered.com/steamrt2/images/>
(look in the `sources/` directory).

The OCI image is built using
[flatdeb-steam](https://gitlab.steamos.cloud/steamrt/flatdeb-steam).
The choice of packages to include in the runtime is partly in the
flatdeb configuration, but mostly controlled by
[the steamrt/soldier branch of the steamrt source package](https://gitlab.steamos.cloud/steamrt/steamrt/-/tree/steamrt/soldier).

Another relevant project is
[steam-runtime-tools](https://gitlab.steamos.cloud/steamrt/steam-runtime-tools),
which includes the `pressure-vessel` container-runner tool, the
`steam-runtime-system-info` diagnostic tool, and some library code that
they share, and also builds the Steampipe depot used to integrate this
runtime into the SteamPlay compatibility tool mechanism.

[guide for game developers]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/slr-for-game-developers.md
[scout SDK]: https://gitlab.steamos.cloud/steamrt/scout/sdk/-/blob/master/README.md
[sniper SDK]: https://gitlab.steamos.cloud/steamrt/sniper/sdk/-/blob/master/README.md
